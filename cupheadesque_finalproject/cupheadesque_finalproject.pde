Table table;
int top_score;

/* The following variable manages which screen to display. */
float gameScreen = 0;

/* The following variable keeps track of score. */
int score = 0;

/* The variable below establishes the countdown time. */
int countdownTime;
timer new_t;
PImage bg;
int currentTime;


PrintWriter output;

int position = 50;
import ddf.minim.*;
Minim minim;
AudioPlayer music;
AudioPlayer weapon_sound;
boolean music_toggle;
boolean weapon_toggle;
sound_button c1;
sound_button c2;
String music_text, weapon_text;

head h;
eye e1;
eye e2;
float theta = 0;
float dir1, dir2;

PImage background;
PShape shield;

player p;
timer t, t2;

ArrayList<bullet> bullets = new ArrayList<bullet>();
ArrayList<bomb> bombs = new ArrayList<bomb>();
boolean bullet_buffer;
boolean bomb_buffer;
int bomb_timer, invul_timer;

int bullet_hits;
int needed;
PImage playerImages [];
int playerFrames;
PImage bombImages [];
int bombFrames;
int hits;
boolean lose = false;


BossTwo b2;

ArrayList<Projectile> p_list = new ArrayList<Projectile>();
Pillar[] pillars;
boolean pause = false;


void makeGame() {
  h = new head(-400, 394, 100);
  e1 = new eye(-400, 394, 100, 0, PI/60);
  e2 = new eye(-400, 394, 100, PI/3, PI/60);
  p = new player(250, 250, 50, 10, 3, 75, 75);
  t = new timer(1000);
  t2 = new timer(1000);
  b2 = new BossTwo(1000, 300, p_list);
  bullets = new ArrayList<bullet>();
  p_list = new ArrayList<Projectile>();
  currentTime = millis();
  pillars[0] = new Pillar(1000, 0);
  hits=0;
  pillars[1] = new ReversePillar(2000, height);
}


void setup() {
  size(1400, 788);
  frameRate(30);
  background = loadImage("JunkyardJive.png");
  background.resize(1400, 788);
  pillars = new Pillar[2];
  makeGame();
  bomb_timer=3;
  invul_timer = 3;
  new_t= new timer(1000);
  playerFrames=12;
  playerImages = new PImage[playerFrames];
  for (int i =0; i < playerFrames; i++) {
    playerImages[i] = loadImage("plane_sprite/cuphead_plane_" + nf(i, 2)+".png");
    playerImages[i].resize(50, 50);
  }

  bombFrames=8;
  bombImages = new PImage[bombFrames];
  for (int i =0; i < bombFrames; i++) {
    bombImages[i] = loadImage("bomb_sprite/bomb_img_" + nf(i, 2)+".png");
    bombImages[i].resize(50, 50);
  }

  needed=10;

  c1 = new sound_button(10, 10, 50, 30, color(110), color(200));
  c2 = new sound_button(70, 10, 50, 30, color(110), color(200));
  minim = new Minim(this);

  music = minim.loadFile("bg_music.mp3");
  weapon_sound = minim.loadFile("gun_sound.wav");

  output = createWriter("Leaderboard.txt");
}

void draw() {
  /* Display contents according to the value of gameScreen. */
  if (gameScreen == 0) { 

    background(255, 100, 0);
    textAlign(CENTER);
    fill(0, 255, 100);
    textSize(85);

    /* Show the title of the game. */
    text("Cuphead-esque ", width/2, height/2);
    textSize(45);

    /* Show the options for the different phases and to start the game. */
    text("Simply click '1 for Phase 1, ", width/2, height-250);
    text("or hit '2' for Phase 2.", width/2, height-190);
    textSize(20); 
    text("Click to start", width/2, height-100);
  } else if (gameScreen == 1) {
    new_t.unpause();


    background(background);

    dir1 = sin(theta);
    dir2 = sin(theta + PI/3);
    theta += PI/60;

    if (music_toggle) {
      music.play();
    } else {
      music.pause();
    }
    //rectMode(CENTER);
    c1.update(mouseX, mouseY);
    c1.display();
    rectMode(CORNER);

    //soundSelect button text
    if (music_toggle == true) {
      music_text = "Music ON";
    } else if (music_toggle == false) {
      music_text = "Music OFF";
    }

    fill(0);
    textSize(8);
    text(music_text, 35, 30); 
    noFill();



    rectMode(CENTER);
    c2.update(mouseX, mouseY);
    c2.display();
    rectMode(CORNER);

    //soundSelect button text
    if (weapon_toggle == true) {
      weapon_text = "SFX ON";
    } else if (weapon_toggle == false) {
      weapon_text = "SFX OFF";
    }

    fill(0);
    textSize(8);
    text(weapon_text, 90, 30); 
    noFill();



    //h.health();
    p.health();
    if (dir1 >= 0 && dir2 >= 0) {
      h.display();
      h.run();
      e1.display();
      e1.move();
      e2.display();
      e2.move();
    } else if (dir1 < 0 && dir2 < 0) {
      e1.display();
      e1.move();
      e2.display();
      e2.move();
      h.display();
      h.run();
    } else if (dir1 >= 0 && dir2 < 0) {
      e2.display();
      e2.move();
      h.display();
      h.run();
      e1.display();
      e1.move();
    } else if (dir2 >= 0 && dir1 < 0) {
      e1.display();
      e1.move();
      h.display();
      h.run();
      e2.display();
      e2.move();
    }
    e1.reposition(h);
    e2.reposition(h);

    p.update();
    p.display();
    for (int i = 0; i < bullets.size(); i++) {
      bullet b = bullets.get(i);
      if (h.hit(b)) {
        h.damage();
        hits++;
      }
      if (b.hit(h)) {
        bullets.remove(i);
      }
    }
    for (int i = 0; i < bombs.size(); i++) {
      bomb bo = bombs.get(i);
      if (h.bombed(bo)) {
        h.bdamage();
      }
      if (bo.hit(h)) {
        bombs.remove(i);
      }
    }
    drawBullet();
    drawBomb();

    if (t.hasElapsed()) {
      t.setCurrent();
      bomb_timer--;
    }

    if (p.collided(h)) {
      p.toggle();
      p.damage();
    }

    if (p.inv() && t2.hasElapsed()) {
      t2.setCurrent();
      invul_timer--;
    }

    if (invul_timer < 0) {
      p.toggle();
      invul_timer = 3;
    }

    //if (bomb_timer < 0) {bomb_buffer=true;}

    ultMeter();

    if (h.dead()) {
      countdownTime = (millis() - currentTime) / 1000;
      gameScreen =3;
    }
    else if (p.dead()) {
      countdownTime = (millis() - currentTime) / 1000;
      gameScreen =3;
      lose = true;
    }
  } else if (gameScreen == 2) {
    new_t.unpause();
    background(background);
    
    
    
    if (music_toggle) {
      music.play();
    } else {
      music.pause();
    }
    //rectMode(CENTER);
    c1.update(mouseX, mouseY);
    c1.display();
    rectMode(CORNER);

    //soundSelect button text
    if (music_toggle == true) {
      music_text = "Music ON";
    } else if (music_toggle == false) {
      music_text = "Music OFF";
    }

    fill(0);
    textSize(8);
    text(music_text, 35, 30); 
    noFill();



    rectMode(CENTER);
    c2.update(mouseX, mouseY);
    c2.display();
    rectMode(CORNER);

    //soundSelect button text
    if (weapon_toggle == true) {
      weapon_text = "SFX ON";
    } else if (weapon_toggle == false) {
      weapon_text = "SFX OFF";
    }

    fill(0);
    textSize(8);
    text(weapon_text, 90, 30); 
    noFill();



    for (int i = 0; i < pillars.length; i++) {
      if (!p.inv() && pillars[i].hit(p)) {
        p.toggle();
        p.damage();
      }
      pillars[i].display();
      pillars[i].move();
    }
    if (!p.inv() && b2.hit(p)) {
      p.toggle();
      p.damage();
    }
    b2.display();
    ArrayList<Projectile> active_p = new ArrayList<Projectile>();
    for (int i = 0; i < p_list.size(); i++) {
      Projectile proj = p_list.get(i);
      if (!p.inv() && proj.hit(p)) {
        p.toggle();
        p.damage();
      } else {
        proj.display();
        proj.move();
        if (proj.isActive()) {
          active_p.add(proj);
        }
      }
    }
    p_list = active_p;
    b2.updateP(p_list);



    dir1 = sin(theta);
    dir2 = sin(theta + PI/3);
    theta += PI/60;
    p.health();

    p.update();
    p.display();
    for (int i = 0; i < bullets.size(); i++) {
      bullet b = bullets.get(i);
      /*if (h.hit(b)) {
       h.damage();
       hits++;
       }
       if (b.hit(h)) {
       bullets.remove(i);
       }*/
      if (b.hitBoss(b2)) {
        bullets.remove(i);
        hits++;
        b2.damage();
      }
    }
    drawBullet();
    drawBomb();

    if (t.hasElapsed()) {
      t.setCurrent();
      bomb_timer--;
    }

    /*if (p.collided(h)) {
     p.toggle();
     p.damage();
     }*/

    if (p.inv() && t2.hasElapsed()) {
      t2.setCurrent();
      invul_timer--;
    }

    if (invul_timer < 0) {
      p.toggle();
      invul_timer = 3;
    }

    //if (bomb_timer < 0) {bomb_buffer=true;}

    ultMeter();

    if (b2.dead) {
      countdownTime = (millis() - currentTime) / 1000;
      gameScreen =3;
    }
    else if (p.dead()) {
      countdownTime = (millis() - currentTime) / 1000;
      gameScreen =3;
      lose = true;
    }
  } else if (gameScreen == 3) { 

    //background(bg);
    textAlign(CENTER);
    fill(236, 240, 241);
    textSize(45);

    t.pause();
    new_t.pause();
    if (lose) {
      text("You Died!", 700, 700);
    }
    else {
      text("You Win!", 700, 700);
    }

    /* Show the score. */
    text("Your Time", width/2, height/2 - 120);
    textSize(130);
    text(String.format("%02d:%02d", countdownTime / 60, countdownTime % 60), width/2, height/2);
    textSize(30);
    
    if (!lose) {
      table = loadTable("Leaderboard.csv", "header");
  
      for (TableRow row : table.rows()) {
        top_score = row.getInt("scores");
      }
  
      if (countdownTime < top_score) {
        TableRow newRow = table.addRow();
        newRow.setInt("scores", countdownTime);
        table.removeRow(0);
        saveTable(table, "Leaderboard.csv");
      }
      
      text("Highest Score", width/2 - 200, height/2 + 70);
      textSize(30);
  
      table = loadTable("Leaderboard.csv", "header");
  
      for (TableRow row : table.rows()) {
        int id = row.getInt("scores");
        text(String.format("%02d:%02d", id / 60, id % 60), 500, 500);
      }
    }

    /* Show the option to restart. */
    text("Click to Restart", width/2, height-30);
  }
}


void drawBullet() {
  for (int i = 0; i<bullets.size(); i++) {
    bullets.get(i).displayBullet();
  }
}

void drawBomb() {
  for (int i = 0; i<bombs.size(); i++) {
    PVector gravity=new PVector(0.1, 1);
    bombs.get(i).applyForce(gravity);
    bombs.get(i).update();
    bombs.get(i).displayBomb();
  }
}

void ultMeter() {  
  noStroke();
  fill(189, 195, 199);
  rectMode(CORNER);
  rect(25, height-100, 5*(needed), 10);
  if (hits >= 10) {
    fill(46, 204, 113);
  } else {
    fill(231, 76, 60);
  }
  rectMode(CORNER);
  if (hits<10) {
    rect(25, height-100, hits*5, 10);
  } else {
    bomb_buffer=true;
    rect(25, height-100, 10*5, 10);
  }
}




void mousePressed() { 
  if (c1.isMouseOver) {
    if (!music_toggle) {
      music_toggle=true;
    } else {
      music_toggle=false;
    }
  }
  if (c2.isMouseOver) {
    if (!weapon_toggle) {
      weapon_toggle=true;
    } else {
      weapon_toggle=false;
    }
  }

  if (gameScreen == 3) {
    makeGame();
    lose = false;
    score = 0;
    gameScreen = 0;
  }
}
void mouseReleased() {
  c1.isReleased();
  c2.isReleased();
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      p.up = true;
    } else if (keyCode == DOWN) {
      p.down = true;
    } else if (keyCode == LEFT) {
      p.left = true;
    } else if (keyCode == RIGHT) {
      p.right = true;
    }
  } 


  if (key == 'x' || key=='X') {
    if (bullet_buffer==true) {
      bullet b = new bullet(p, 25, 10);
      bullets.add(b);
      bullet_buffer=false;
      if (weapon_toggle) {
        if (!weapon_sound.isPlaying()) {
          weapon_sound.rewind(); 
          weapon_sound.play();
        }
      }
    }
  }


  if (key == 'c' || key=='C') {
    if (bomb_buffer==true) {
      bomb b = new bomb(p, 50);
      bombs.add(b);
      bomb_buffer=false;
      hits=0;
    }
  }


  /* If the value of gameScreen is 0, set it equal to 2. */
  if (key=='1') {
    if (gameScreen == 0) { 

      gameScreen = 1;
    }
  }
  if (key=='2') {
    if (gameScreen == 0) { 

      gameScreen = 2;
    }
  }

  /* If the user presses p (either lowercase or uppercase), pause the game and display the mentioned contents. */
  if (key=='p' || key=='P') {

    textAlign(CENTER);
    textSize(70);

    /* Show the following text. */
    text("Game Paused", width/2, height/2);

    textSize(40);
    fill(255);

    /* Show the following pieces of text. */
    text("Press either 'u' or 'U' to unpause. ", width/2, height/2 + 70);
    text("Click either 'q' or 'Q' to completely quit.", width/2, height/2 + 120);

    /* Stop the loop to pause. */
    noLoop();
  }

  /* If the user presses c (either lowercase or uppercase), continue the game. */
  if (key=='u' || key=='U') {

    /* Resume the loop. */
    loop();
  }

  /* If the user presses q (either lowercase or uppercase), completely quit the game. */
  if (key == 'q' || key=='Q') {

    /* Exit the game. */
    //exit();
    gameScreen=0;
  }
}

void keyReleased() {
  if (keyCode == UP) {
    p.up = false;
  } else if (keyCode == DOWN) {
    p.down = false;
  } else if (keyCode == LEFT) {
    p.left = false;
  } else if (keyCode == RIGHT) {
    p.right = false;
  } 

  if (key == 'x' || key=='X') {
    bullet_buffer=true;
  }
  if (key == 'g') {
    p.onGod();
  }
}
