class RedGem extends Gem {
  
  RedGem(float x, float y) {
    super(x, y);
    num_projectiles = 8;
    gem_frames = new PImage[gem_frames_num];
    for (int i = 0; i < gem_frames.length; i++) {
      gem_frames[i] = loadImage("RedGem/inventor_red_gem_00" + nf((i + 1), 2) + ".png");
    }
  }
  
  
  void projectile(ArrayList<Projectile> p_list) {
    for(int i = 0; i < num_projectiles; i++) {
      // num_projectles - 1 due to starting at 0 i.e. the first destY 
      // at i=0 will be the highest possible value at -(height * (spread - 1)) and last destY at i=5 will be lowest possible value at height * spread
      // top most should be -.25 height, and bottom most should be 1.25 height)
      //float destY = -(height * (spread - 1)) + i * (height * spread / (num_projectiles - 1));
      float destY = sin(-spread + i * (2 * spread / (num_projectiles - 1))) * radius + y; 
      float destX = -(cos(-spread + i * (2 * spread / (num_projectiles - 1))) * radius) + x;
      p_list.add(new RedProjectile(x, y, destX, destY));
    }
  }
}
