class Gem {
  
  float x, y;
  PImage[] gem_frames;
  int gem_frames_num = 8;
  int num_projectiles = 0;
  float spread = PI / 2;
  float xBound = -200;
  float radius;
  
  Gem(float x, float y) {
    this.x = x;
    this.y = y;
    this.radius = x - xBound;
  }

  void display(int altFrameCount) {
    image(gem_frames[altFrameCount % gem_frames_num], x, y);
  }
  
  void projectile(ArrayList<Projectile> p_list) {
  }
}
