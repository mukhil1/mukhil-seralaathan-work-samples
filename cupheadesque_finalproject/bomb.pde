class bomb {
  PVector l, v, a;
  float size;
  int currentFrame, loopFrames,delay;
  
  bomb(player p, float r) {
    
    l = new PVector(p.x+10, p.y);
    v=new PVector(5,3);
    a=new PVector(0,0);
    this.size=r;
    currentFrame=0;
    loopFrames=8;
    delay=0;
  }
  
  void applyForce(PVector force) {
    PVector f = PVector.div(force,size);
    a.add(f);
  }
  
  void update() {
    v.add(a);
    l.add(v);
    a.mult(0);
    
    if (delay==0) {
      currentFrame=(currentFrame+1)%loopFrames;
    }
    delay=(delay+1)%4;
  }
    
  
  void displayBomb() {
    fill(0);
   // ellipse(l.x, l.y, size, size);
    imageMode(CENTER);
    image(bombImages[currentFrame], l.x, l.y, size,size);
  }
  
  boolean hit(head h) {
    if (dist(l.x, l.y, h.x - 50, h.y) <= 100 || dist(l.x, l.y, h.x + 50, h.y) <= 100) {
      return true;
    } else if (l.y > 788) {
      return true;
    }
    return false;
  }
}
