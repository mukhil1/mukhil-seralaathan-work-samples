class RedProjectile extends Projectile{
  
  RedProjectile(float x, float y, float destX, float destY) {
 
    super(x, y, destX, destY);
    projectile_frames = new PImage[projectile_frames_num];
    steps = (int) random(90, 135);
    for (int i = 0; i < projectile_frames.length; i++) {
      projectile_frames[i] = loadImage("RedGemProjectile/inventor_gem_red_bullets_001900" + nf((i + 1), 2) + ".png");
    }
    this.sizeX = projectile_frames[0].width;
    this.sizeY = projectile_frames[0].height;
  }
  
}
