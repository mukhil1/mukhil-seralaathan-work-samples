class player {
  float x, y, vx, vy, r, hp;
  boolean up, down, left, right;
  float speed;
  float theta = 0;
  float theta_speed = PI/36;
  boolean invul = false;
  int sizeX, sizeY;
  float oldHP;
  
  int currentFrame, loopFrames, offset,delay;
  
   player(float _x,float _y, float _r, float _s, float _hp, int sizeX, int sizeY) {
    playerImages = new PImage[playerFrames];
    for (int i =0; i < playerFrames; i++) {
      playerImages[i] = loadImage("plane_sprite/cuphead_plane_" + nf(i,2)+".png");
      playerImages[i].resize(sizeX, sizeY);
    }
  
    this.x = _x;
    this.y = _y;
    this.r = _r;
    this.hp = _hp;
    this.sizeX = sizeX;
    this.sizeY = sizeY;
    speed = _s;
    vx = 0;
    vy = 0;
    currentFrame=0;
    loopFrames=4;
    offset=0;
    delay=0;
  }
  
  void update() {
    if (up) {
      vy=-speed;
      offset=8;
    }
    
    if (down) {
      vy=speed;
      offset=4;
    }
    
    if (right) {
      vx=speed;
    }
    
    if (left) {
      vx= -speed;
    }
    
    if ((right && left) || (!right && !left)) {
      vx=0;
    }
    
    if ((up && down) || (!up && !down)) {
      vy=0;
    }
    
    if (vx==0 && vy==0) {
      offset=0;
    }
    if (invul) {
      theta += theta_speed;
    }
    
    x+=vx;
    y+=vy;
    if (delay==0) {
      
      currentFrame=(currentFrame+1)%loopFrames;
    }
    delay=(delay+1)%4;
    
    
    walls();
  }
  
  void walls() {
    if (x>width-r/2) {
      x=width-r/2;
    }
    if (x<r/2) {
      x=r/2;
    }
    if (y>height-r/2) {
      y=height-r/2;
    }
    if (y<r/2) {
      y=r/2;
    }
    
  }
  
  void display() {  
    imageMode(CENTER);
    image(playerImages[currentFrame+offset], x, y);
    if (invul) {
      shield = createShape();
      shield.beginShape();
      shield.fill(#21F6FF);
      shield.vertex(x + (30 * cos(theta)) - 5, y + (30 * sin(theta)) - 5);
      shield.vertex(x + (30 * cos(theta)), y + (30 * sin(theta)) - 4);
      shield.vertex(x + (30 * cos(theta)) + 5, y + (30 * sin(theta)) - 5);
      shield.vertex(x + (30 * cos(theta)) + 5, y + (30 * sin(theta)) + 5);
      shield.vertex(x + (30 * cos(theta)), y + (30 * sin(theta)) + 8);
      shield.vertex(x + (30 * cos(theta)) - 5, y + (30 * sin(theta)) + 5);
      shield.endShape();
      shape(shield);
    }
  }
  
  void toggle() {
    invul = !invul;
  }
  
  boolean inv() {
    return invul;
  }
  
  boolean collided(head h) {
    if (!invul) {
      if ((dist(x, y, h.x - 50, h.y) < (100 + r/2)) || (dist(x, y, h.x, h.y) < (100 + r/2)) || (dist(x, y, h.x + 50, h.y) < (100 + r/2))) {
        return true;
      }
    }
    return false;
  }
  
  void damage() {
    hp--;
    if (hp < 0) {
      hp = 0;
    }
  }
  
  void onGod() {
    if (hp > 3) {
      hp = oldHP;
    } 
    else {
      oldHP = hp;
      hp = 999;
    }
  }
  
  void health() {
    textSize(20);
    text("HP: " + str(hp), 50, 748);
  }
  
  boolean dead() {
    if (hp==0) {
      return true;
    }
    return false;
  }
  
}
