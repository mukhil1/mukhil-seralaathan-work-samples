class BlueProjectile extends Projectile{
  
  BlueProjectile(float x, float y, float destX, float destY) {
    super(x, y, destX, destY);
    steps = (int) random(85, 115);
    projectile_frames = new PImage[projectile_frames_num];
    for (int i = 0; i < projectile_frames.length; i++) {
      projectile_frames[i] = loadImage("BlueGemProjectile/inventor_gem_blue_bullets_00" + nf((i + 1), 2) + ".png");
    }
    this.sizeX = projectile_frames[0].width;
    this.sizeY = projectile_frames[0].height;
  }
  
  
}
