class BossTwo {
  
  boolean appear = true;
  boolean attack = false;
  boolean attacking = false;
  boolean reverseAttack = false;
  boolean attackBlue = true;
  boolean attackRed = false;
  int appear_frames_num = 16;
  PImage[] appear_frames;
  int idle_frames_num = 16;
  PImage[] idle_frames;
  int attack_frames_num = 9;
  PImage[] attack_blue_frames;
  PImage[] attack_red_frames;
  int attacking_frames_num = 16;
  PImage[] attacking_frames;
  PImage[] death1_frames;
  PImage[] death2_frames;
  int death1_frames_num = 21;
  int death2_frames_num = 19;
  boolean dying = false;
  boolean dead = false;
  boolean death2 = false;
  float x, y;
  int altFrameCount;
  int idleCount = 0;
  int idle_max = 2;
  int attackingCount = 0;
  int attackingMax = 5;
  int movealtFrameCount = 0;
  int movealtFrameCountThresh = 250;
  int moveCount = 0;
  int moveMax = 72;
  int currentWidth = 0;
  int currentHeight = 0;
  BlueGem bg;
  boolean moveDown;
  boolean moveUp;
  int speed = 3;
  int hp = 200;
  int sizeX, sizeY;
  RedGem rg;
  ArrayList<Projectile> p_list;
  boolean damaged = false;
  
  
  BossTwo(float x, float y,  ArrayList<Projectile> p_list) {
    this.x = x;
    this.y = y;
    appear_frames = new PImage[appear_frames_num];
    for (int i = 0; i < appear_frames.length; i++) {
    appear_frames[i] = loadImage("Appears/inventor_intro_00" + nf((i + 1), 2) + ".png");
    }
    idle_frames = new PImage[idle_frames_num];
    for (int i = 0; i < idle_frames.length; i++) {
    idle_frames[i] = loadImage("Idle/inventor_idle_00" + nf((i + 1), 2) + ".png");
    }
    attack_blue_frames = new PImage[attack_frames_num];
    for (int i = 0; i < attack_blue_frames.length; i++) {
    attack_blue_frames[i] = loadImage("AttackBlue/inventor_gem_attack_blue_00" + nf((i + 1), 2) + ".png");
    }
    attack_red_frames = new PImage[attack_frames_num];
    for (int i = 0; i < attack_red_frames.length; i++) {
    attack_red_frames[i] = loadImage("AttackRed/inventor_gem_attack_red_00" + nf((i + 1), 2) + ".png");
    }
    attacking_frames = new PImage[attacking_frames_num];
    for (int i = 0; i < attacking_frames.length; i++) {
    attacking_frames[i] = loadImage("Attacking/inventor_gem_attack_00" + nf((i + 10), 2) + ".png");
    }
    death1_frames = new PImage[death1_frames_num];
    for (int i = 0; i < death1_frames.length; i++) {
      if (i >= 13) {
        death1_frames[i] = loadImage("Defeat/inventor_death_00" + nf((i + 2), 2) + ".png");
      }
      else {
        death1_frames[i] = loadImage("Defeat/inventor_death_00" + nf((i + 1), 2) + ".png");
      }
    }
    death2_frames = new PImage[death2_frames_num];
    for (int i = 0; i < death2_frames.length; i++) {
    death2_frames[i] = loadImage("Defeat (Explosion)/inventor_death_explosion_00" + nf((i + 1), 2) + ".png");
    }
    this.p_list = p_list;
    bg = new BlueGem(x - 125, y + 150);
    rg = new RedGem(x - 125, y + 150);
    this.sizeX = idle_frames[0].width;
    this.sizeY = idle_frames[1].height;
  }
  
  void appear() {
    image(appear_frames[altFrameCount % appear_frames_num], x, y);
    if(appear_frames_num - altFrameCount == 1) {
      appear = false;
      altFrameCount = 0;
    }
  }
  
  void idle() {
    image(idle_frames[altFrameCount % idle_frames_num], x, y);
    if(idle_frames_num - altFrameCount % idle_frames_num == 1) {
      idleCount++; 
    }
  }
  
  void attackBlue() {
    if (reverseAttack) {
      image(attack_red_frames[attack_frames_num - altFrameCount], x, y);
      if (attack_frames_num - altFrameCount <= 0) {
        reverseAttack = false;
      }
    }
    else if (attacking) {
      attacking(bg);
    } else{
      image(attack_blue_frames[altFrameCount % attack_frames_num], x, y);
      if(attack_frames_num - altFrameCount % attack_frames_num == 1) {
        attacking = true;
        //altFrameCount = 0;
      }
    }
  }
  
  void attackRed() {
    if (reverseAttack) {
      image(attack_blue_frames[attack_frames_num - altFrameCount], x, y);
      if (attack_frames_num - altFrameCount <= 0) {
        reverseAttack = false;
      }
    }
    else if (attacking) {
      attacking(rg);
    } else {
      image(attack_red_frames[altFrameCount % attack_frames_num], x, y);
      if(attack_frames_num - altFrameCount % attack_frames_num == 1) {
        attacking = true;
        altFrameCount = 0;
      }
    }
  }
  
  void attacking(Gem g) {
    image(attacking_frames[altFrameCount % attacking_frames_num], x, y);
      g.display(altFrameCount);
      if(attacking_frames_num - altFrameCount % attacking_frames_num == 1) {
        g.projectile(p_list);
        attackingCount++;
        if (attackingCount > attackingMax) {
          attacking = false;
          attack = false;
          attackingCount = 0;
          attackBlue = !attackBlue;
          attackRed = !attackRed;
          reverseAttack = true;
          altFrameCount = 0;
        }
      }
  }
  
  void updateP(ArrayList<Projectile> p_list) {
    this.p_list = p_list;
  }
  
  void move() {
    if (moveDown) {
      this.y += speed;
      bg.y += speed;
      rg.y += speed;
    } else{
      this.y -= speed;
      bg.y -= speed;
      rg.y -= speed;
    }
    moveCount++;
    if (moveCount >= moveMax) {
      moveCount = 0;
      moveUp = false;
      moveDown = false;
    }
  }
  
  void onDeath() {
    if (death2) {
      image(death2_frames[altFrameCount], x, y);
      if (death2_frames_num - altFrameCount <= 1) {
        dead = true;
      }
    } else {
      image(death1_frames[altFrameCount], x, y);
      if (death1_frames_num - altFrameCount <= 1) {
        death2 = true;
        altFrameCount = 0;
      }
    }
  }
  
  void die() {
    dying = true;
    altFrameCount = 0;
  }
  
  boolean hit(player p) {
    // the -40 acts as a buffer
    if (dist(0, y, 0, p.y) < max(p.sizeY, sizeY) - 40 && dist(x, 0, p.x, 0) < max(p.sizeX, sizeX) - 40) {
      return true;
    }
    return false;
  }
  
  void damage() {
    hp--;
    damaged = true;
    if (hp <= 0) {
      die();
    }
  }
  
  void display() {
    if (dead) {
      return;
    }
    if (dying) {
      onDeath();
      altFrameCount++;
      return;
    }
    if (damaged) {
      tint(220, 50);
      damaged = false;
    } 
    else {
      tint(255, 255);
    }
    if (movealtFrameCount > movealtFrameCountThresh) {
      movealtFrameCountThresh = (int) random(200, 350);
      movealtFrameCount = 0;
      if (y > height / 4) {
        moveUp = true;
      } else if (y < height / 4) {
        moveDown = true;
      } else {
        moveDown = random(100) > 50 ? true : false;
        moveUp = !moveDown;
    }
    }
    if (moveDown || moveUp ) {
      move();
    }
    if (appear) {
      appear();
    }
    else if(idleCount > idle_max) {
      attack = true;
      idleCount = 0;
      altFrameCount = 0;
    }
    if(attack || reverseAttack) {
      if(attackBlue) {
        attackBlue();
      } else {
        attackRed();
      }
    }
    else if(!appear) {
      idle();
    }
    tint(255, 255);
    altFrameCount++;
    movealtFrameCount++;
  }
}
