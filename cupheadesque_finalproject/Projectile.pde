class Projectile {
  
  float x, y, initialX, initialY, destX, destY;
  int sizeX, sizeY;
  int steps = 100;
  PImage [] projectile_frames;
  int projectile_frames_num = 20;
  boolean active = true;
  
  Projectile(float x, float y, float destX, float destY) {
    this.x = x;
    this.y = y;
    this.initialX = x;
    this.initialY = y;
    this.destX = destX;
    this.destY = destY;
  }
  
  void display() {
    image(projectile_frames[frameCount % projectile_frames_num], x, y);
  }
  
  boolean isActive() {
    return active;
  }
  
  void move() {
    x += (float) (destX - initialX) / steps;
    y += (float) (destY - initialY) / steps;
    if (x < -200 || y > height + 200 || y < -200) {
      active = false;
    }
  }
  
  boolean hit(player p) {
    // the -40 acts as a buffer
    if (dist(0, y, 0, p.y) < max(p.sizeY, sizeY) - 40 && dist(x, 0, p.x, 0) < max(p.sizeX, sizeX) - 40) {
      active = false;
      return true;
    }
    return false;
  }
}
