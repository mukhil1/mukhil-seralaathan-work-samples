1) Ensure all files are in the folder, including the Excel file used to record the least possible time taken by a player to finish the game.
2) Make sure to close the Excel file before beginning the program.
3) Install Minim library for Sound (Sketch -> Import Library -> Add Library -> Minim).
4) Click the play button to run.
5) To enter God-Mode (999 health), click ‘g’.
6) Choose boss 1 or boss 2 by clicking 1 or 2 on the main menu.
7) Use arrow keys, ‘x’, and ‘c’, to control character and shoot at the boss
8) Upon victory/defeat, an end screen shows up displaying the time taken and the overall least amount of time the player has taken to finish the game. 
9) ‘q’ returns to the main menu, ‘p’ pauses, and ‘u’ unpauses. Sound is controlled by the buttons at the top left.
