class timer {
  float interval;
  float current;
  boolean run=true;
  
  timer(float _i) {
    interval=_i;
  }
  
  void setCurrent() {
    current=millis();
  }
  
  boolean hasElapsed() {
    if (run==true && (millis() - current) >= interval) {
        return true;
      }
    else {
      return false;  
    }
  }
  
  void pause() {
    run=false; 
  }
  
  void unpause() {
    run=true;
  }
    
}
