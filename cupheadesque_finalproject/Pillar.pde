class Pillar {
  float x, y;
  int sizeX, sizeY;
  PImage p;
  
  Pillar(float x, float y) {
    p = loadImage("Pillar.png");
    this.x = x;
    this.y = y;
    this.sizeX = p.width;
    this.sizeY = p.height;
  }
  
  void move() {
    this.x -= 15;
    if (x <= -100) {
      x = 2000;
    }
  }
  
  void display() {
    image(p, x, y);
  }
  
  boolean hit(player p) {
    // the -40 acts as a buffer
    if (dist(0, y, 0, p.y) < max(p.sizeY, sizeY) / 2 && dist(x, 0, p.x, 0) < max(p.sizeX, sizeX) / 2) {
      return true;
    }
    return false;
  }
  
}
