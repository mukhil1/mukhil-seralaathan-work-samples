class head {
  float x, y, hp;
  float inc = 20;
  boolean alive = true;
  PImage[] lhead_frames;
  PImage[] rhead_frames;
  
  
  head(float x, float y, float hp) {
    this.x = x;
    this.y = y;
    this.hp = hp;
    /* lhead_frames = new PImage[16];
    for (int i = 0; i < lhead_frames.length; i++) {
      lhead_frames[i] = loadImage("RobotLeft/robot_left_spin-" + nf((i + 1), 2) + ".png");
    }
    rhead_frames = new PImage[16];
    for (int i = 0; i < rhead_frames.length; i++) {
      rhead_frames[i] = loadImage("RobotRight/robot_right_spin-" + nf((i + 1), 2) + ".png");
    } */
  }
  
  void display() {
    if (alive) {
      /* imageMode(CENTER);
      if (inc > 0) {
        image(rhead_frames[frameCount % 16], x, y);
      }
      if (inc < 0) {
        image(lhead_frames[frameCount % 16], x, y);
      } */
      
      noStroke();
      fill(131);
      rectMode(CENTER);
      rect(x, y, 100, 200);
      ellipse(x + 50, y, 200, 200);
      ellipse(x - 50, y, 200, 200);
      stroke(20);
    }
  }
  
  void move() {
    if (alive) {
      x += inc;
    }
  }
  
  void reposition() {
    if (x >= 1900 || x <= -500) {
      inc = -inc;
      y = random(0, 788);
    }
  }
  
  void run() {
    move();
    reposition();
  }
  
  void damage() {
    noStroke();
    fill(200);
    rectMode(CENTER);
    rect(x, y, 100, 200);
    ellipse(x + 50, y, 200, 200);
    ellipse(x - 50, y, 200, 200);
    rectMode(CORNER);
    stroke(20);
    hp--;
    if (hp < 0) {
      hp = 0;
      alive = false;
    }
  }
  
  void bdamage() {
    noStroke();
    fill(200);
    rectMode(CENTER);
    rect(x, y, 100, 200);
    ellipse(x + 50, y, 200, 200);
    ellipse(x - 50, y, 200, 200);
    rectMode(CORNER);
    stroke(20);
    hp -= 10;
    if (hp < 0) {
      hp = 0;
    }
  }
  
  void health() {
    fill(#46E852);
    rectMode(CORNER);
    rect(1350 - (13 * hp), 50, (13 * hp), 50);
  }
  
  boolean hit(bullet b) {
    if (dist(b.x, b.y, x - 50, y) <= 100) {
      return true;
    }
    return false;
  }
  
  boolean bombed(bomb bo) {
    if (dist(bo.l.x, bo.l.y, x - 50, y) <= 100 || dist(bo.l.x, bo.l.y, x + 50, y) <= 100) {
      return true;
    }
    return false;
  }
  
  boolean dead() {
    if (!alive) {
      return true;  
    }
    return false;
  }
}
