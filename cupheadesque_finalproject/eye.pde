class eye extends head {
  float theta, speed;
  boolean right = true;
  float inc = 20;
  
  eye(float x, float y, float hp, float theta, float speed) {
    super(x, y, hp);
    this.theta = theta;
    this.speed = speed;
  }
  
  void display() {
    stroke(20);
    fill(255);
    if (right) {
      ellipse(x + 40, y + (100 * cos(theta)), 50, 50);
    } else {
      ellipse(x - 40, y + (100 * cos(theta)), 50, 50);
    }
  }
  
  void reposition(head h) {
    if (x >= 1900 || x <= -500) {
      inc = -inc;
      y = h.y;
      right = !right;
    }
  }
  
  void move(){
    x += inc;
    theta += speed;
  }
}
