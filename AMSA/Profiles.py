


AMSADatabaseFileName = "Documents/CS/AMSA/AMSA Database (2022-2023) - Database (alpha).csv"
File1 = "Documents/CS/AMSA/Member Info/A-Local Dues Status.txt"
File2 = "Documents/CS/AMSA/Member Info/B-Meeting Points.txt"
File3 = "Documents/CS/AMSA/Member Info/C-MedResources Points.txt"
File4 = "Documents/CS/AMSA/Member Info/D-Social Points.txt"
File5 = "Documents/CS/AMSA/Member Info/E-Volunteering Points.txt"
File6 = "Documents/CS/AMSA/Member Info/F-Fundraising Points.txt"
File7 = "Documents/CS/AMSA/Member Info/G-Bonus Points.txt"
File8 = "Documents/CS/AMSA/Member Info/H-Total Points.txt"
File9 = "Documents/CS/AMSA/Member Info/I-Member Status.txt"


DatabaseData = []
with open(AMSADatabaseFileName, mode='r') as csv_file:
    for row in csv_file:
        rowrow = row.split(',')
        DatabaseData.append(rowrow)

ProfileUsernames = []
MemberStatus = []
LocalDuesStatus = []
TotalPoints = []
MeetingPoint = []
MedResPoints = []
SocialPoints = []
VolunteerPoints = []
FundrasingPoints = []
BonusPoints = []


## HTML for MemberStatus

outfile = open(File9,"w")
count = 1
print('Member Status')
for row in DatabaseData:
    if row[7] == '':
        count+=1
        continue
    else:
        outfile.write('[O_U user_name="{0}"][get_sheet_value location="Database (alpha)!O{1}"] [/O_U]\n'.format(row[7], count))
    count += 1
outfile.write('</strong>\n')
outfile.close()


outfile = open(File1, "w")
count = 1
print('Local Dues Status')
for row in DatabaseData:
    if row[7] == '':
        count+=1
        continue
    else:
        outfile.write('[O_U user_name="{0}"][get_sheet_value location="Database (alpha)!Y{1}"] [/O_U]\n'.format(row[7], count))
    count += 1
outfile.write('</strong>\n')
outfile.close()

outfile = open(File8, "w")
count = 1
print('Total Points')
for row in DatabaseData:
    if row[7] == '':
        count+=1
        continue
    else:
        outfile.write('[O_U user_name="{0}"][get_sheet_value location="Database (alpha)!P{1}"] [/O_U]\n'.format(row[7], count))
    count += 1
outfile.write('</strong>\n')
outfile.close()


outfile = open(File2, "w")
count = 1
print('Meeting Points')
for row in DatabaseData:
    if row[7] == '':
        count+=1
        continue
    else:
        outfile.write('[O_U user_name="{0}"][get_sheet_value location="Database (alpha)!Q{1}"] [/O_U]\n'.format(row[7], count))
    count += 1
outfile.write('</strong>\n')
outfile.close()


outfile = open(File3, "w")
count = 1
print('Med Resource Points')
for row in DatabaseData:
    if row[7] == '':
        count+=1
        continue
    else:
        outfile.write('[O_U user_name="{0}"][get_sheet_value location="Database (alpha)!R{1}"] [/O_U]\n'.format(row[7], count))
    count +=  1
outfile.write('</strong>\n')
outfile.close()


outfile = open(File4, "w")
count = 1
print('Social Points')
for row in DatabaseData:
    if row[7] == '':
        count+=1
        continue
    else:
        outfile.write('[O_U user_name="{0}"][get_sheet_value location="Database (alpha)!S{1}"] [/O_U]\n'.format(row[7], count))
    count += 1
outfile.write('</strong>\n')
outfile.close()


outfile = open(File5, "w")
count = 1
print('Volunteering Points')
for row in DatabaseData:
    if row[7] == '':
        count+=1
        continue
    else:
        outfile.write('[O_U user_name="{0}"][get_sheet_value location="Database (alpha)!T{1}"] [/O_U]\n'.format(row[7], count))
    count += 1
outfile.write('</strong>\n')
outfile.close()


outfile = open(File6, "w")
count = 1
print('Fundraising Points')
for row in DatabaseData:
    if row[7] == '':
        count+=1
        continue
    else:
        outfile.write('[O_U user_name="{0}"][get_sheet_value location="Database (alpha)!U{1}"] [/O_U]\n'.format(row[7], count))
    count += 1
outfile.write('</strong>\n')
outfile.close()


outfile = open(File7, "w")
count = 1
print('Bonus Points')
for row in DatabaseData:
    if row[7] == '':
        count+=1
        continue
    else:
        outfile.write('[O_U user_name="{0}"][get_sheet_value location="Database (alpha)!V{1}"] [/O_U]\n'.format(row[7], count))
    count += 1
outfile.write('</strong>\n')
outfile.close()
