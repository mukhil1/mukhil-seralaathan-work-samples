class Walls {

  float Health = 100;
  int MaximumHealth = 100;
  float DecreaseinHealth = 1;
  int WidthofHealthBar = 60;
  int SpeedofWall = 5;
  int IntervalofWall = 1000;
  float LastAddTime = 0;
  int MinimumGapHeight = 200;
  int MaximumGapHeight = 300;
  int WidthofWall = 80;
  color wallColor = color(200, 255, 220);
  ArrayList<int[]> walls = new ArrayList<int[]>();

  /* The following code defines the constructor. */
  Walls(player _p) {
  
    p = _p;
  
  }

  /* This method draws the walls, which serve as obstacles and decrease the health and life of the ball. */
  void DrawWalls(int index) {
    
    int[] wall = walls.get(index);
    
    /* This establishes the gap wall settings. */
    int GapWall_Horizontal = wall[0];
    int GapWall_Vertical = wall[1];
    int GapWall_Width = wall[2];
    int GapWall_Height = wall[3];
    
    /* Draw the actual walls. */
    rectMode(CORNER);
    fill(wallColor);
    rect(GapWall_Horizontal, 0, GapWall_Width, GapWall_Vertical);
    rect(GapWall_Horizontal, GapWall_Vertical + GapWall_Height, GapWall_Width, height - (GapWall_Vertical + GapWall_Height));
  
  }
  
  /* This method moves the walls. */
  void MoveWalls(int index) {
    
    int[] wall = walls.get(index);
    
    /* Decrease the speed of the wall. */
    wall[0] -= SpeedofWall;
    
  }
  
  /* This method removes the walls. */
  void RemoveWalls(int index) {
    
    int[] wall = walls.get(index);
    
    if (wall[0]+wall[2] <= 0) {
      
      walls.remove(index);
    
    }
    
  }
  
  /* The following method makes further modifications to the walls. */
  void HandleWalls(float Ball_Horizontal, float Ball_Size, float Ball_Vertical) {
    
    for (int i = 0; i < walls.size(); i++) {
      
      RemoveWalls(i);
      MoveWalls(i);
      DrawWalls(i);
      Detect_Collision_of_Wall_and_Ball(i, Ball_Horizontal, Ball_Size, Ball_Vertical);
    }
    
  }
  
  /* The method below recognizes a collision between a wall and ball. */
  void Detect_Collision_of_Wall_and_Ball(int index, float Ball_Horizontal, float Ball_Size, float Ball_Vertical) {
    
    int[] wall = walls.get(index);
    
    /* Receive the gap wall settings.*/
    int GapWall_Horizontal = wall[0];
    int GapWall_Vertical = wall[1];
    int GapWall_Width = wall[2];
    int GapWall_Height = wall[3];
    int wallScored = wall[4];
    int wallTop_Horizontal = GapWall_Horizontal;
    int wallTop_Vertical = 0;
    int wallTop_Width = GapWall_Width;
    int wallTop_Height = GapWall_Vertical;
    int wallBottom_Horizontal = GapWall_Horizontal;
    int wallBottom_Vertical = GapWall_Vertical + GapWall_Height;
    int wallBottom_Width = GapWall_Width;
    int wallBottom_Height = height - (GapWall_Vertical + GapWall_Height);
  
    if (
      (Ball_Horizontal + (Ball_Size/2) > wallTop_Horizontal) &&
      (Ball_Horizontal - (Ball_Size/2) < wallTop_Horizontal + wallTop_Width) &&
      (Ball_Vertical + (Ball_Size/2) > wallTop_Vertical) &&
      (Ball_Vertical -(Ball_Size/2)< wallTop_Vertical + wallTop_Height)
      ) {
        
        /* The ball collides with an upper wall. */
        DecreaseHealth();
        
    }
    
    if (
      (Ball_Horizontal + (Ball_Size/2) > wallBottom_Horizontal) &&
      (Ball_Horizontal - (Ball_Size/2) < wallBottom_Horizontal + wallBottom_Width) &&
      (Ball_Vertical +(Ball_Size/2) > wallBottom_Vertical) &&
      (Ball_Vertical -(Ball_Size/2)< wallBottom_Vertical + wallBottom_Height)
      ) {
      
        /* The ball collides with a lower wall. */
        DecreaseHealth();
        
    }
  
    if (Ball_Horizontal > GapWall_Horizontal + (GapWall_Width/2) && wallScored==0) {
      
      wallScored = 1;
      wall[4] = 1;
      
    }
    
  }
    
    /* The following method adds walls. */
    void AddWalls() {
    
      if (millis() - LastAddTime > IntervalofWall) {
        
        int RandomHeight = round(random(MinimumGapHeight, MaximumGapHeight));
        int RandomYCoordinate = round(random(0, height - RandomHeight));
        // {gapWallX, gapWallY, gapWallWidth, gapWallHeight}
        int[] RandomWall = {width, RandomYCoordinate, WidthofWall, RandomHeight, 0}; 
        walls.add(RandomWall);
        LastAddTime = millis();
        
      }
      
    }
    
  void HealthBar(float Ball_Horizontal, float Ball_Vertical) {
    
    noStroke();
    fill(189, 195, 199);
    rectMode(CORNER);
    rect(Ball_Horizontal - (WidthofHealthBar/2), Ball_Vertical - 30, WidthofHealthBar, 5);
    
    if (Health > 60) {
      
      fill(46, 204, 113);
      
    } 
    
    else if (Health > 30) {
      
      fill(230, 126, 34);
      
    } 
    
    else {
      
      fill(231, 76, 60);
    }
    
    rectMode(CORNER);
    rect(Ball_Horizontal-(WidthofHealthBar/2), Ball_Vertical - 30, WidthofHealthBar * (Health/MaximumHealth), 5);
  
  }
  
  /* The method below decreases the health of a ball. */
  void DecreaseHealth() {
    
    Health -= DecreaseinHealth;
    
    if (Health <= 0) {
      
      /* Reset value of gameAction to 2. */
      gameAction = 2;
      
    }
    
    }
  }
