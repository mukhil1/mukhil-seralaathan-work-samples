class player {
  float x, y, vx, vy, r;
  boolean up, down, left, right;
  float speed;
  
  player(float _x,float _y, float _r, float _s) {
    this.x = _x;
    this.y = _y;
    this.r = _r;
    speed = _s;
    vx = 0;
    vy = 0;
  }
  
  void update() {
    if (up) {
      vy=-speed;
    }
    
    if (down) {
      vy=speed;
    }
    
    if (right) {
      vx=speed;
    }
    
    if (left) {
      vx= -speed;
    }
    
    if ((right && left) || (!right && !left)) {
      vx=0;
    }
    
    if ((up && down) || (!up && !down)) {
      vy=0;
    }
    
    x+=vx;
    y+=vy;
    
    walls();
  }
  
  void walls() {
    if (x>width-r/2) {
      x=width-r/2;
    }
    if (x<r/2) {
      x=r/2;
    }
    if (y>height-r/2) {
      y=height-r/2;
    }
    if (y<r/2) {
      y=r/2;
    }
    
  }
  
  void display() {  
    fill(255,255,0);
    ellipse(x,y,r,r);
  }
  
}
