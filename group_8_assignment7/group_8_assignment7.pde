/* Initialize all relevant variables. */

player p;
timer t;
Walls w;
goal g;
int gameAction = 0;
int score = 0;
int countdownTime;
int totalTime;

void setup() {
  size(800,800);
  /* Create new instances of classes. */
  p = new player(250,250, 25, 5);
  t = new timer(1000);
  w = new Walls(p);
  g = new goal(p);
 
  /* Initially establish the countdown time as 15 seconds. */
  countdownTime=15;
}

 //<>// //<>//
void draw()  {
  /* Introduce the game if gameAction is 0. */
  if (gameAction == 0) { 
    t.pause();
    printStartScreen();
  } 
  
  /* Play the game if gameAction is 1. */
  else if (gameAction == 1) { 
    t.unpause();
    background(255);
    
    g.display();
    
    p.update();
    p.display();
    
    w.AddWalls();
    w.HandleWalls(p.x, p.r, p.y);
    
    w.HealthBar(p.x, p.y);
    
    printGUI();
    
    
    /* The following defines the timer countdown display. */
  if (t.hasElapsed()) {  
    if (countdownTime > 0)  {
      countdownTime--;
      totalTime++;
      t.setCurrent();
    }
    else if (countdownTime <= 0) {
      gameAction = 2; /* Reset gameAction to 2. */
    }
  }
  }
  
  /* Display the score and an option to restart if gameAction is 2. */
  else if (gameAction == 2) { 
    countdownTime=15;
    
    p.x = p.y = 250;
    
    printScore();
    
  }
  
  // if the player hits the goal, reset the countdown and move the goal
  if (g.contacted()) {
    g.relocate();  
    countdownTime=15;
    t.setCurrent();
    score++;
  }
  
   //<>//
}

/* The following method prints the welcome/start screen. */
void printStartScreen() {
  background(255);
  textAlign(CENTER);
  fill(255, 100, 100);
  textSize(68);
  text("Flappy Bird with a Twist", width/2, height/2-60);
  textSize(25);
  text("Move your character using the arrow keys", width/2, height/2);
  text("Collect 10 stars quickly while avoiding the walls", width/2, height/2+40);
  textSize(20); 
  text("Click to start", width/2, height-30);
}

/* The following method prints the GUI screen. */
void printGUI() {
  fill(255,0,0);
  textAlign(LEFT);
  textSize(12);
  text("Score: " + score, 50, 25);
  text("Time Remaining: " + countdownTime, 50, 50);
  text("Health : " + w.Health, 50, 75);
}


/* The following method prints the death screen. */
void printScore(){
  background(0);
  textSize(54);
  textAlign(CENTER);
  
  // win condition: score >= 10 ; display different messages if we win/lose
  if (score>=10) { 
    fill(0,255,0);
    text("YOU WIN!", width/2, height/2-150);
    text("CONGRATULATIONS!", width/2, height/2-100); 
  }
  else {
    fill(255,0,0);
    text("YOU LOSE!", width/2, height/2-150);
    text("BETTER LUCK NEXT TIME", width/2, height/2-100); 
  }
  
  fill(200, 200, 200);
  text("Your Score", width/2, height/2);
  text(score, width/2, height/2+50);
  
  textSize(30);
  text("You Survived for " + totalTime + " seconds", width/2, height/2+150);
  
  textSize(25);
  text("Click to Restart", width/2, height-30);
}





void mousePressed() {
  /* If we are on the initial screen when clicked, start the game. */
  if (gameAction == 0) {     
    gameAction = 1;
  }
  
  /* Restart the game in this case. */
  if (gameAction == 2) {
    /* Restart values. */
    totalTime = 0;
    score = 0;
    w.Health = 100;
    w.LastAddTime = 0;
    w.walls = new ArrayList<int[]>();
    countdownTime=15;
    gameAction = 1;
  }
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      p.up = true;
    } 
    else if (keyCode == DOWN) {
      p.down = true;
    } 
    else if (keyCode == LEFT) {
      p.left = true;
    } 
    else if (keyCode == RIGHT) {
      p.right = true;
    }   
  } 
  
  if (key=='p' || key=='P') {
    textAlign(CENTER);
    textSize(36);   
    text("Game Paused\nClick 'u' to unpause or 'q' to quit", width/2, height/2);  
    noLoop();  
  }
  
  if (key=='u' || key=='U') {
    loop();
  }
  
  if (key == 'q' || key=='Q') {
    exit();
  }
  
}

void keyReleased() {
  if (keyCode == UP) {
    p.up = false;
  }  
  else if (keyCode == DOWN) {
    p.down = false;
  }   
  else if (keyCode == LEFT) {
    p.left = false;
  }   
  else if (keyCode == RIGHT) {
    p.right = false;
  } 
}
