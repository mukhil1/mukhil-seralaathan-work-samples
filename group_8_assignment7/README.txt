1) Make sure player, timer, goal, and Walls tabs/classes are in the folder
2) Click Run 
3) The player is met with a start screen. Click anywhere on screen to start the game
4) To pause, press “p” or “P”; to unpause press “u” or “U” ; to exit press “q” or “Q”
5) Use arrow keys to move the yellow character around
6) Move over the red stars to increase score and avoid green walls to preserve health. Stars must be collected within 15 seconds of each other or time runs out.
7) When time runs out or health is 0 a death screen is displayed showing score and time survived. If score>=10 a win message is shown, otherwise a loss message is shown. Click anywhere on screen to play again or “q/Q” to exit
