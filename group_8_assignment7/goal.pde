class goal {
  PShape star;
  float x, y, xo, yo;
  float theta = 0;
  
  // takes only the player for the contacted method
  goal(player _p) {
    p = _p;
    x = random(0, 800);
    y = random(0, 800);
  }
  
  // displays the goal as a star and spins it in place
  void display() {
    pushMatrix();
    translate(x, y);
    rotate(theta);
    star = createShape();
    star.beginShape();
    noStroke();
    fill(#FF0000);
    star.vertex(-12, 20);
    star.vertex(0, -20);
    star.vertex(12, 20);
    star.vertex(-20, -5);
    star.vertex(20, -5);
    star.endShape();
    shape(star);
    popMatrix();
    theta += PI/100;
  }
  
  // checks if the player has touched the star's center
  Boolean contacted() {
    if (dist(p.x, p.y, x, y) <= p.r) {
      return true;
    }
    return false;
  }
  
  // moves the goal to a new, randomized location
  void relocate() {
    xo = x;
    yo = y;
    while (dist(x, y, xo, yo) < 400) { // makes sure the new goal is at least 400 pixels away
      x = random(0, 800);
      y = random(0, 800);
    }
  }
}
